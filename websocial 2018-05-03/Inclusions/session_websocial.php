<?php session_start();
		if(!isset($_SESSION['connexion'])){
			$_SESSION['connexion']=false;
		}

		if(!isset($_SESSION['id'])){
			$_SESSION['id']='';
		}

		if(!isset($_SESSION['pseudo'])){
			$_SESSION['pseudo']='';
		}

		//Ouverture de la base de données.
		$_SESSION['basewebsocial']= new mysqli( 'p:localhost' , 'root' , null , 'linkece' );
		if (0!=$_SESSION['basewebsocial']->connect_errno) {
			echo "Echec lors de la connexion à la base 'linkece' : (" . $_basewebsocial->connect_errno . ") " . $_basewebsocial->connect_error. "\n";
			die;
			}
		$_SESSION['basewebsocial']->set_charset('utf8');
?>
<html>
	<head>
		<!--<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">-->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="Inclusions\style_websocial.css">
		<script src="Inclusions\jquery-3.3.1.js"></script>
	</head>
	<body>
		<div id="en-tete">
			<div class="en-tete-gauche">
				<center>
					<img src="Inclusions\logo ECE.PNG" alt="logo ECE" height="68" border="0">
				</center>
			</div>
			<div class="en-tete-centre">
					Link'ECE Paris
			</div>
			<div class="en-tete-droite">
				<br>
				<center>
					<?php
						if ($_SESSION['connexion'])
						{
							echo 'Bonjour '.$_SESSION['pseudo'];
						}
					?>
				</center>
			</div>
		</div>
	
		<div id="navigation">
			<a href="#">ACCUEIL</a> | <a href="#">MON RESEAU</a>  | <a href="Vous.php">MON PROFIL</a> | <a href="#">NOTIFICATIONS</a> | <a href="#">EMPLOIS</a> | <a href="#">MESSAGERIE</a></div>
	</body>
</html>
