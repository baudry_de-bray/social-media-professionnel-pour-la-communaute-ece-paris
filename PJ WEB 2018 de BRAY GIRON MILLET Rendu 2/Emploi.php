<?php include "Inclusions\session_websocial.php"; ?>
<html>

	<head>
		<title>Web social ECE - EMPLOI</title>

		<style type="text/css">
<!--		.emetteur{
				color:blue;
				text-align: right;
			}
			
			.destinataire{
				color:red;
				text-align: left;
			}
			.hide{
				visibility:hidden;
			}
			.show{
				visibility:visible;
			}
-->		</style>

	</head>

	<body>


	<div id="logo">
		Emploi
	</div>
	
	<div id="affichage">
		<div class="col-gauche hide">
			<p>&nbsp;</p>
			
				Titre : <span id="titre"></span><BR>
				
				ID annonce : <span id="idannonce"></span><BR>
				
				Date de publication : <span id="datepublication"></span><BR><BR>
				
				Annonce : <BR><BR><span id="annonce"></span><BR>

		</div>
		<div class="col-milieu">
		<BR>
		<BR>
				<?php $connexion=new mysqli( 'p:localhost' , 'root' , null , 'linkece' );
				$sql = "SELECT *
				FROM emploi
				ORDER BY datepublication DESC";
				$res = $connexion->query($sql);

		
				while($row = $res->fetch_assoc()){
				?>
				<BR>
					<div>
						<?php echo $row['titre']." "; 
								
							  echo $row['idannonce']." ";
							  echo $row['datepublication']." ";
							  
						?>
						<input type="button" value="Afficher Annonce" onclick="return afficherAnnonce(<?php echo $row['idannonce'] ?>);return false;">
						<input type="button" value="Supprimer Annonce" onclick="return supprimerAnnonce(<?php echo $row['idannonce'] ?>);return false;">
						
					</div>
				<?php
				}
		
				$connexion->close();
				?>
		</div>
		<div class="col-droite">
			<p>&nbsp;</p>
			<p> Ajouter une nouvelle annonce :</p>
			<form id="AjouterAnnonce">
			<input type="text" name="titre" value="Saisir titre">
			<textarea name="annonce" cols="40" rows="10">Saisir l'annonce</textarea>
			<input type="hidden" name="action" value="Ajouter_Annonce">
			<input type="submit" value="Ajouter l'annonce">
			</form>

			</div>

		</div>
	</div>

	
	<script>
	
	function afficherAnnonce(idannonce){
		$.post("Emploi_gestion.php",{idannonce: idannonce, action: 'Recuperer_Annonce'},function(data){
			$('.col-gauche #titre').html(data['titre']);
			$('.col-gauche #idannonce').html(data['idannonce']);
			$('.col-gauche #datepublication').html(data['datepublication']);
			$('.col-gauche #annonce').html(data['annonce']);
			$('.col-gauche').removeClass('hide');
			$('.col-gauche').addClass('show');
		},'json');
				return false;
	}
	function supprimerAnnonce(idannonce){
		if(confirm('Voulez-vous vraiment supprimer ?')){
			$.post("Emploi_gestion.php",{idannonce: idannonce, action: 'Supprimer_Annonce'},function(data){
				document.location.reload();
			},'json');
		}
				return false;
	}
		
	$("#AjouterAnnonce").submit(function(){
		$.post("Emploi_gestion.php",$("#AjouterAnnonce").serialize(),function(data){
			document.location.reload();
		},'json');
		return false;
	});
	
	</script>
	</body>

</html>