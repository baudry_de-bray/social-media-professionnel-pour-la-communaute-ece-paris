<?php session_start();
		if(!isset($_SESSION['connexion'])){
			$_SESSION['connexion']=false;
		}

		if(!isset($_SESSION['id'])){
			$_SESSION['id']='';
		}

		if(!isset($_SESSION['pseudo'])){
			$_SESSION['pseudo']='';
		}

		if(!isset($_SESSION['rafraichir'])){
			$_SESSION['rafraichir']=false;
		}

		if(!isset($_SESSION['action'])){
			$_SESSION['action']="";
		}

		//Ouverture de la base de données.
		$_SESSION['basewebsocial']= new mysqli( 'p:localhost' , 'root' , null , 'linkece' );
		if (0!=$_SESSION['basewebsocial']->connect_errno) {
			echo "Echec lors de la connexion à la base 'linkece' : (" . $_basewebsocial->connect_errno . ") " . $_basewebsocial->connect_error. "\n";
			die;
			}
		$_SESSION['basewebsocial']->set_charset('utf8');
?>
<html>
	<head>
		<!--<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">-->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="Inclusions\style_websocial.css">
		<script src="Inclusions\jquery-3.3.1.js"></script>
	</head>
	<body>
		<div id="en-tete">
			<div class="en-tete-gauche">
				<center>
					<img src="Inclusions\logo ECE.PNG" alt="logo ECE" height="68" border="0">
				</center>
			</div>
			<div class="en-tete-centre">
					Link'ECE Paris
			</div>
			<div class="en-tete-droite">
				<br>
				<center>
					<div>
						<?php
							if (!Empty($_POST)){
								if(isset($_POST['boutonconnexion'])){
									$_SESSION['connexion']=false;
									$_SESSION['id']='';
									$_SESSION['pseudo']='';
									$_SESSION['rafraichir']=false;
									$_SESSION['action']="";
								}
							}
							if ($_SESSION['connexion'])
							{
								echo '<div id="boutondeconnexion">';
								echo '<form action="Index.php" method="post">';
								echo 'Bonjour '.$_SESSION['pseudo'].' ';
								echo '<input type="submit" name="boutonconnexion" value="X" />';
								echo '</form>';
								echo '</div>';
							}
						?>
					</div>
				</center>
			</div>
		</div>
	
		<div id="navigation">
			<?php
				if ($_SESSION['connexion']){
					echo '<a href="Accueil.php">ACCUEIL</a> | <a href="MonReseau.php">MON RESEAU</a>  | <a href="Vous.php">MON PROFIL</a> | <a href="Notifications.php">NOTIFICATIONS</a> | <a href="Emploi.php">EMPLOIS</a> | <a href="Messagerie.php">MESSAGERIE</a>';
				}
				else{
					echo '<BR>';
				}
					
			?>
		</div>
	</body>
</html>
