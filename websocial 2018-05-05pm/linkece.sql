-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  sam. 05 mai 2018 à 13:47
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `linkece`
--

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

DROP TABLE IF EXISTS `auteur`;
CREATE TABLE IF NOT EXISTS `auteur` (
  `email` varchar(64) COLLATE utf8_bin NOT NULL,
  `pseudo` varchar(64) COLLATE utf8_bin NOT NULL,
  `nom` varchar(64) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(64) COLLATE utf8_bin NOT NULL,
  `sexe` varchar(1) COLLATE utf8_bin NOT NULL,
  `photo` longblob,
  `imagefond` longblob,
  `CV` longblob,
  `administrateur` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'booléen',
  PRIMARY KEY (`email`,`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `connecter`
--

DROP TABLE IF EXISTS `connecter`;
CREATE TABLE IF NOT EXISTS `connecter` (
  `email1` varchar(64) COLLATE utf8_bin NOT NULL,
  `email2` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'identifiant d''un ami de email 1. Email2 a donné son amitié à email1. Email1 a le droit de voir les infos privées de email2',
  PRIMARY KEY (`email1`,`email2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `emploi`
--

DROP TABLE IF EXISTS `emploi`;
CREATE TABLE IF NOT EXISTS `emploi` (
  `idannonce` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) COLLATE utf8_bin NOT NULL,
  `datepublication` date NOT NULL,
  `annonce` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idannonce`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
CREATE TABLE IF NOT EXISTS `evenement` (
  `email` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheurepublication` datetime NOT NULL,
  `photovideo` longblob NOT NULL,
  `dateheurephoto` datetime NOT NULL,
  `commentaire` text COLLATE utf8_bin NOT NULL,
  `sentiment` varchar(11) COLLATE utf8_bin NOT NULL,
  `prive` tinyint(4) NOT NULL,
  PRIMARY KEY (`email`,`dateheurepublication`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `evenementposte`
--

DROP TABLE IF EXISTS `evenementposte`;
CREATE TABLE IF NOT EXISTS `evenementposte` (
  `email` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheurepublication` datetime NOT NULL,
  `emailposte` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheureposte` datetime NOT NULL,
  `commentaire` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`email`,`dateheurepublication`,`emailposte`,`dateheureposte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `mailemetteur` varchar(64) COLLATE utf8_bin NOT NULL,
  `maildestinataire` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheure` datetime NOT NULL,
  `texte` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`mailemetteur`,`maildestinataire`,`dateheure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
