-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 30 Avril 2018 à 09:40
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `websocial`
--

-- --------------------------------------------------------

--
-- Structure de la table `auteur`
--

CREATE TABLE IF NOT EXISTS `auteur` (
  `email` varchar(64) COLLATE utf8_bin NOT NULL,
  `pseudo` varchar(64) COLLATE utf8_bin NOT NULL,
  `nom` varchar(64) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(64) COLLATE utf8_bin NOT NULL,
  `sexe` varchar(1) COLLATE utf8_bin NOT NULL,
  `photo` blob NOT NULL,
  `imagefond` blob NOT NULL,
  `CV` blob NOT NULL,
  `administrateur` tinyint(1) NOT NULL COMMENT 'booléen',
  PRIMARY KEY (`email`,`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `connecter`
--

CREATE TABLE IF NOT EXISTS `connecter` (
  `email1` varchar(64) COLLATE utf8_bin NOT NULL,
  `email2` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`email1`,`email2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `emploi`
--

CREATE TABLE IF NOT EXISTS `emploi` (
  `idannonce` int(11) NOT NULL,
  `titre` varchar(64) COLLATE utf8_bin NOT NULL,
  `datepublication` date NOT NULL,
  `annonce` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idannonce`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE IF NOT EXISTS `evenement` (
  `email` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheurepublication` datetime NOT NULL,
  `photovideo` blob NOT NULL,
  `dateheurephoto` datetime NOT NULL,
  `commentaire` text COLLATE utf8_bin NOT NULL,
  `sentiment` int(11) NOT NULL,
  `prive` tinyint(4) NOT NULL,
  PRIMARY KEY (`email`,`dateheurepublication`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `evenementposte`
--

CREATE TABLE IF NOT EXISTS `evenementposte` (
  `email` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheurepublication` datetime NOT NULL,
  `emailposte` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheureposte` datetime NOT NULL,
  `commentaire` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`email`,`dateheurepublication`,`emailposte`,`dateheureposte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `mailemetteur` varchar(64) COLLATE utf8_bin NOT NULL,
  `maildestinataire` varchar(64) COLLATE utf8_bin NOT NULL,
  `dateheure` datetime NOT NULL,
  `texte` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`mailemetteur`,`maildestinataire`,`dateheure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
