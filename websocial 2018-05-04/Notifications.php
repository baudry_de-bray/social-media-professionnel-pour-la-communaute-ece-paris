<?php include "Inclusions\session_websocial.php"; ?>
<html>

	<head>
		<title>Web social ECE - NOTIFICATIONS</title>
		<script>
			$(document).ready(function(){
				$("#commenter").click(function(){
					$("#formulaire").show();
					$("#nouvellephoto").attr("alt", $("#commenter").attr("value"));
				});
				$("#hideformulaire").click(function(){
					$("#formulaire").hide();
				});
			});
		</script>
		<script>
			function loadpic(files){
				$photoblob=window.URL.createObjectURL(files[0]);
				$("#nouvellephoto").attr("src", $photoblob);
				window.URL.revokeObjectURL(files[0]);
			}
		</script>
	</head>

	<body>
	<?php
	/*Si l'on arrive sur la page pour par le bouton des mises à jour du formulaire, on met à jour la base.*/
		$affichage_commentaire='style="display: none;"';
		$titre_commentaire="";
		$email_commentaire="";
		$dateheurepublication_commentaire="";
	/*Si l'on arrive sur la page pour par le bouton de demande de saisie d'un commentaire, on affiche le formulaire.*/
		if (!Empty($_POST)){
			if ($_POST['soumission']=="Commenter"){
				$affichage_commentaire="";
				$email_commentaire=$_POST['evenement-email'];
				$dateheurepublication_commentaire=$_POST['evenement-dateheurepublication'];
				$titre_commentaire=$_POST['evenement-titre'];
			}

	/*Si l'on arrive sur la page pour par le bouton d'ajout d'un commentaire, on met à jour la base.*/
			if (($_POST['soumission']=="Publier") AND ($_SESSION['rafraichir'])){
				$affichage_commentaire='style="display: none;"';
				$Query="INSERT INTO evenementposte (email, dateheurepublication, emailposte, dateheureposte, commentaire)";
				$Query=$Query." VALUES ('".$_POST['evenement-email']."', '".$_POST['evenement-dateheurepublication']."'";
				$Query=$Query.", '".$_SESSION['id']."', now()";
				$Query=$Query.", '".$_POST['commentaire']."'";
				$Query=$Query.")";
				$Resultat_Query = $_SESSION['basewebsocial']->query($Query);
				if (0!=$_SESSION['basewebsocial']->errno) {
					echo "Erreur d'insertion sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
					die;
				}
				$_SESSION['rafraichir']=false;
			}
		}
	?>
	<?php
	/*Chargement des événements à afficher : ceux qui sont publics et ceux qui émanent d'amis.*/ 
		$Query = "SELECT evenement.email, dateheurepublication, photovideo, dateheurephoto, commentaire, sentiment, prive, prenom, nom ";
		$Query = $Query." FROM evenement INNER JOIN auteur ON evenement.email=auteur.email WHERE (NOT evenement.email='".$_SESSION['id']."') ";
		$Query = $Query."AND (NOT prive)";
		$Query = $Query."UNION ";
		$Query = $Query."SELECT evenement.email, dateheurepublication, photovideo, dateheurephoto, commentaire, sentiment, prive, prenom, nom ";
		$Query = $Query." FROM evenement INNER JOIN connecter ON evenement.email=email2 ";
		$Query = $Query." INNER JOIN auteur ON evenement.email=auteur.email WHERE (email1='".$_SESSION['id']."') ";
		$Query = $Query." ORDER BY dateheurepublication DESC;";
		$Resultat_Query = $_SESSION['basewebsocial']->query($Query);
		if (0!=$_SESSION['basewebsocial']->errno) {
			echo "Erreur de query sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
			die;
		}
	?>

	<div id="logo">
		Notifications
	</div>
	
	<div id="affichage">
		<div class="col-gauche">
			<p>&nbsp;</p>
			<div class="element-de-menu">
				<a href="#">Article Un</a></div>
			<div class="element-de-menu">
				<a href="#">Article Deux</a></div>
			<div class="element-de-menu">
				<a href="#">ArticleTrois</a></div>
			<div class="element-de-menu">
				<a href="#">Article Quatre</a></div>
			<p>&nbsp;</p>
		</div>
		<div class="col-milieu" style="text-align: left;">
			&nbsp;
			<?php
				$evenement=$Resultat_Query->fetch_assoc();
				while($evenement){
					echo "<BR>";
					list($date, $time) = explode(" ", $evenement['dateheurepublication']);
					list($year, $month, $day) = explode("-", $date);
					list($hour, $min, $sec) = explode(":", $time);
					$titre="Publié le ".$day."/".$month."/".$year." à ".$hour.":".$min." ";
					$titre=$titre."par ".$evenement['prenom']." ".$evenement['nom'];
					echo $titre;
					if($evenement['prive']){
						echo " (privé).";
					}
					else {
						echo " (public).";
					}
					echo "<BR>";
					echo '<img src="data:image/jpeg;base64,'.base64_encode($evenement['photovideo']).'" width="100%" border="2"><br>';
					echo '<form action="Notifications.php" method="post">';
					list($date, $time) = explode(" ", $evenement['dateheurephoto']);
					list($year, $month, $day) = explode("-", $date);
					list($hour, $min, $sec) = explode(":", $time);
					echo "Photo prise le ".$day."/".$month."/".$year." ";
					echo "(".$evenement['sentiment'].") ";
					$where=" WHERE email='".$evenement['email']."' AND dateheurepublication=".$evenement['dateheurepublication'];
					echo '<input type="hidden" name="evenement-email" value="'.$evenement['email'].'" />';
					echo '<input type="hidden" name="evenement-dateheurepublication" value="'.$evenement['dateheurepublication'].'" />';
					echo '<input type="hidden" name="evenement-titre" value="'.$titre.'" />';
					echo '<input type="submit" name="soumission" value="Commenter" />';
					echo "<BR>".$evenement['commentaire']."<BR>";					
					echo '</form>';

					echo '<div style="text-align: right;">';
	/*Chargement des commentaires liés à l'événement que l'on vient d'afficher.*/ 
						$Query = "SELECT * FROM evenementposte INNER JOIN auteur ON emailposte=auteur.email";
						$Query = $Query." WHERE evenementposte.email='".$evenement['email']."' AND dateheurepublication='".$evenement['dateheurepublication']."'";
						$Query = $Query." ORDER BY dateheureposte DESC";
						$Resultat_Query2 = $_SESSION['basewebsocial']->query($Query);
						if (0!=$_SESSION['basewebsocial']->errno) {
							echo "Erreur de query sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
							die;
						}

						$poste=$Resultat_Query2->fetch_assoc();
						while($poste){
							list($date, $time) = explode(" ", $poste['dateheureposte']);
							list($year, $month, $day) = explode("-", $date);
							list($hour, $min, $sec) = explode(":", $time);
							$titre=$poste['prenom']." ".$poste['nom'];
							$titre=$titre." le ".$day."/".$month."/".$year." à ".$hour.":".$min." ";
							echo $titre.'<BR>';
							echo $poste['commentaire'].'<BR>';
						
							$poste=$Resultat_Query2->fetch_assoc();
						}
					echo '</div>';
					
					$evenement=$Resultat_Query->fetch_assoc();
				}
			?>
		</div>
		<div class="col-droite">
			<?php
				echo '<div id="formulaire" '.$affichage_commentaire.'>';
			?>
					<form enctype="multipart/form-data" action="Notifications.php" method="post">
						<?php
							echo $titre_commentaire;
							echo '<input type="hidden" name="evenement-email" value="'.$email_commentaire.'" />';
							echo '<input type="hidden" name="evenement-dateheurepublication" value="'.$dateheurepublication_commentaire.'" />';
						?>
							Commentaire :
						<textarea name="commentaire" rows="4" cols="30"></textarea>
						<BR>
					<?php
						if ($affichage_commentaire==''){
							echo '<input type="submit" name="soumission" value="Publier" />';
							$_SESSION['rafraichir']=true;
						}
						else{
							echo '<input type="submit" name="soumission" value="Nerienfaire" />';
							$_SESSION['rafraichir']=false;
						}
					?>
					</form>
				</div>
		</div>
	</div>

	</body>

</html>