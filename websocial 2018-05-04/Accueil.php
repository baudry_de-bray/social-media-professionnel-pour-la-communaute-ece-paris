<?php include "Inclusions\session_websocial.php"; ?>
<html>

	<head>
		<title>Web social ECE - ACCUEIL</title>
		<script>
			$(document).ready(function(){
				$("#showformulaire").click(function(){
					$("#formulaire").show();
					$("#showformulaire").hide();
				});
				$("#hideformulaire").click(function(){
					$("#formulaire").hide();
					$("#showformulaire").show();
				});
			});
		</script>
		<script>
			function loadpic(files){
				$photoblob=window.URL.createObjectURL(files[0]);
				$("#nouvellephoto").attr("src", $photoblob);
				window.URL.revokeObjectURL(files[0]);
			}
		</script>
	</head>

	<body>
	<?php
	/*Si l'on arrive sur la page pour par le bouton des mises à jour du formulaire, on met à jour la base.*/
		if ((!Empty($_POST)) && ($_SESSION['rafraichir']==false))
		{
			if (!Empty($_FILES['photo']['tmp_name'])){
				$photoblob = addslashes(file_get_contents ($_FILES['photo']['tmp_name']));
				$newphoto=true;
			}
			else{
				$newphoto=false;
			}

			
			if ($newphoto){
				$Query="INSERT INTO evenement (email, dateheurepublication, photovideo, commentaire, dateheurephoto, sentiment, prive)";
				$Query=$Query." VALUES ('".$_SESSION['id']."', now()";
				$Query=$Query.", '".$photoblob."'";
				$Query=$Query.", '".$_POST['commentaire']."'";
				$Query=$Query.", '".$_POST['datephoto']."'";
				$Query=$Query.", '".$_POST['humeur']."'";
				$Query=$Query.", ".'("'.$_POST['event'].'"="true")';
				$Query=$Query.")";
				$Resultat_Query = $_SESSION['basewebsocial']->query($Query);
				if (0!=$_SESSION['basewebsocial']->errno) {
					echo "Erreur d'insertion sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
					die;
				}
			}
		}
	?>
	<?php
		$Resultat_Query = $_SESSION['basewebsocial']->query("SELECT * FROM evenement WHERE email='".$_SESSION['id']."' ORDER BY dateheurepublication DESC;");
		if (0!=$_SESSION['basewebsocial']->errno) {
			echo "Erreur de query sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
			die;
		}
	?>

	<div id="logo">
		Accueil
	</div>
	
	<div id="affichage">
		<div class="col-gauche">
			<p>&nbsp;</p>
			<div class="element-de-menu">
				<a href="#">Article Un</a></div>
			<div class="element-de-menu">
				<a href="#">Article Deux</a></div>
			<div class="element-de-menu">
				<a href="#">ArticleTrois</a></div>
			<div class="element-de-menu">
				<a href="#">Article Quatre</a></div>
			<p>&nbsp;</p>
		</div>
		<div class="col-milieu" style="text-align: left;">
			&nbsp;
			<?php
				$evenement=$Resultat_Query->fetch_assoc();
				while($evenement){
					echo "<BR>";
					list($date, $time) = explode(" ", $evenement['dateheurepublication']);
					list($year, $month, $day) = explode("-", $date);
					list($hour, $min, $sec) = explode(":", $time);
					echo "publié le ".$day."/".$month."/".$year." à ".$hour.":".$min." ";
					if($evenement['prive']){
						echo "(privé)";
					}
					else {
						echo "(public)";
					}
					echo "<BR>";
					echo '<img src="data:image/jpeg;base64,'.base64_encode($evenement['photovideo']).'" heigh="250" border="2"><br>';
					list($date, $time) = explode(" ", $evenement['dateheurephoto']);
					list($year, $month, $day) = explode("-", $date);
					list($hour, $min, $sec) = explode(":", $time);
					echo "photo prise le ".$day."/".$month."/".$year." ";
					echo "(".$evenement['sentiment'].")<BR>";					
					echo $evenement['commentaire']."<BR>";

					echo '<div style="text-align: right;">';
	/*Chargement des commentaires liés à l'événement que l'on vient d'afficher.*/ 
						$Query = "SELECT * FROM evenementposte INNER JOIN auteur ON emailposte=auteur.email";
						$Query = $Query." WHERE evenementposte.email='".$evenement['email']."' AND dateheurepublication='".$evenement['dateheurepublication']."'";
						$Query = $Query." ORDER BY dateheureposte DESC";
						$Resultat_Query2 = $_SESSION['basewebsocial']->query($Query);
						if (0!=$_SESSION['basewebsocial']->errno) {
							echo "Erreur de query sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
							die;
						}

						$poste=$Resultat_Query2->fetch_assoc();
						while($poste){
							list($date, $time) = explode(" ", $poste['dateheureposte']);
							list($year, $month, $day) = explode("-", $date);
							list($hour, $min, $sec) = explode(":", $time);
							$titre=$poste['prenom']." ".$poste['nom'];
							$titre=$titre." le ".$day."/".$month."/".$year." à ".$hour.":".$min." ";
							echo $titre.'<BR>';
							echo $poste['commentaire'].'<BR>';
						
							$poste=$Resultat_Query2->fetch_assoc();
						}
					echo '</div>';

					
					$evenement=$Resultat_Query->fetch_assoc();
				}
			?>
		</div>
		<div class="col-droite">
			<button id="showformulaire">Créer un événement</button>

			<div id="formulaire" style="display: none;">
				<form enctype="multipart/form-data" action="Accueil.php" method="post">
					<CENTER>
						<input id="choixphoto" type ="file" name="photo" accept="image/*" onchange="loadpic(this.files)">
						<img id="nouvellephoto" src="" alt="Choisissez une photogaphie" width="50" border="1"><br>
					</CENTER>
					<BR>
					<div class="question">
						Date de la photo :&nbsp;
					</div>
					<div class="reponse">
						<input type="date" name="datephoto" />
					</div>
					<BR>
					<div class="question">
						Commentaire :
					</div>
					<textarea name="commentaire" rows="4" cols="30"></textarea>
					<BR>
					<div class="question">
						Votre humeur :&nbsp;
					</div>
					<div class="reponse">
						<select name="humeur" >
							<option value="Heureux">Heureux</option>
							<option value="Content">Content</option>
							<option value="Triste">Triste</option>
						</select>
					</div>
					</BR>
					<div class="question">
						Evénement :&nbsp;
					</div>
					<div class="reponse">
						<input type="radio" name="event" value="true" checked > privé &nbsp;
						<input type="radio" name="event" value="false" > public
					</div>
					<input type="submit" value="Publier" />
				</form>
			</div>

		</div>
	</div>

	</body>

</html>