<?php include "Inclusions\session_websocial.php"; ?>
<html>

	<head>
		<title>Web social ECE - VOUS</title>
		<style type="text/css" media="screen"><!--
.question 
	{
	text-align: right;
	width: 30%;
	float: left
	}

.reponse
	{
	text-align: left;
	width: 70%;
	float: left
	}
-->
		</style>
		<script>
			$(document).ready(function(){
				$("#showformulaire").click(function(){
					$("#formulaire").show();
					$("#showformulaire").hide();
				});
				$("#hideformulaire").click(function(){
					$("#formulaire").hide();
					$("#showformulaire").show();
				});
			});
		</script>
		<script>
			function loadpic(files){
				$photoblob=window.URL.createObjectURL(files[0]);
				$("#nouvellephoto").attr("src", $photoblob);
				window.URL.revokeObjectURL(files[0]);
			}
		</script>
	</head>

	<body>
	<?php
	/*Si l'on arrive sur la page pour par le bouton des mises à jour du formulaire, on met à jour la base.*/
		if ((!Empty($_POST))&& ($_SESSION['rafraichir']==false))
		{
			if (!Empty($_FILES['photo']['tmp_name'])){
				$photoblob = addslashes(file_get_contents ($_FILES['photo']['tmp_name']));
				$newphoto=true;
			}
			else{
				$newphoto=false;
			}

			if (!Empty($_FILES['cv']['tmp_name'])){
				$cvblob = addslashes(file_get_contents ($_FILES['cv']['tmp_name']));
				$newcv=true;
			}
			else{
				$newcv=false;
			}
			
			$Query="UPDATE auteur SET nom='".$_POST['nom']."', prenom='".$_POST['prenom']."', email='".$_POST['email']."'";
			$Query=$Query.", pseudo='".$_POST['pseudo']."'";
			$Query=$Query.", sexe='".$_POST['sexe']."'";
			if ($newphoto){
				$Query=$Query.", photo='".$photoblob."' ";
			}
			if ($newcv){
				$Query=$Query.", cv='".$cvblob."' ";
			}
			$Query=$Query." WHERE email='".$_SESSION['id']."'";
			$Resultat_Query = $_SESSION['basewebsocial']->query($Query);
			if (0!=$_SESSION['basewebsocial']->errno) {
				echo "Erreur de query sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
				die;
			}
			$_SESSION['id']=$_POST['email'];
			$_SESSION['pseudo']=$_POST['pseudo'];
			$_SESSION['rafraichir']=true;
			header('Location: Vous.php');

		}
		$_SESSION['rafraichir']=false;
	?>
	<?php
		$Resultat_Query = $_SESSION['basewebsocial']->query("SELECT * FROM auteur WHERE email='".$_SESSION['id']."';");
		if (0!=$_SESSION['basewebsocial']->errno) {
			echo "Erreur de query sur la base 'websocial' : (" . $_SESSION['basewebsocial']->errno . ") " . $_SESSION['basewebsocial']->error. "\n";
			die;
		}
		$auteur=$Resultat_Query->fetch_assoc();
	?>

	<div id="logo">
		Vous
	</div>
	
	<div id="affichage">
		<div class="col-gauche">
			<p>&nbsp;</p>
			<div class="element-de-menu">
				<a href="#">Article Un</a></div>
			<div class="element-de-menu">
				<a href="#">Article Deux</a></div>
			<div class="element-de-menu">
				<a href="#">ArticleTrois</a></div>
			<div class="element-de-menu">
				<a href="#">Article Quatre</a></div>
			<p>&nbsp;</p>
		</div>
		<div class="col-milieu">
			<?php
				echo '<CENTER>';
				echo '<img src="data:image/jpeg;base64,'.base64_encode($auteur['photo']).'" alt="Ajouter votre portrait" width="250" border="2"><br>';
				echo $auteur['prenom']." ".$auteur['nom']."<BR>";
				echo '</CENTER>';
				echo 'email : '.$auteur['email']."<BR>";
				if ($auteur['sexe']=="M"){
					$sexe="Masculin";
				}
				else{				
					$sexe="Féminin";
				}
				echo 'sexe : '.$sexe."<BR>";
			?>
		</div>
		<div class="col-droite">
			<button id="showformulaire">Mettre à jour vos informations</button>

			<div id="formulaire" style="display: none;">
				<form enctype="multipart/form-data" action="Vous.php" method="post">
					<div class="question">
						Nom :&nbsp;
					</div>
					<div class="reponse">
						<input type="text" name="nom" value=<?php echo $auteur['nom'] ?> />
					</div>
					<div class="question">
						Prenom :&nbsp;
					</div>
					<div class="reponse">
						<input type="text" name="prenom" value=<?php echo $auteur['prenom'] ?> />
					</div>
					<div class="question">
						Pseudo :&nbsp;
					</div>
					<div class="reponse">
						<input type="text" name="pseudo" value=<?php echo $auteur['pseudo'] ?> />
					</div>
					<BR><BR><BR><BR>
					<CENTER>
						<input id="choixphoto" type ="file" name="photo" accept="image/*" onchange="loadpic(this.files)">
						<img id="nouvellephoto" src="" alt="Choisissez une photogaphie" width="50" border="1"><br>
					</CENTER>
					<BR>
					<div class="question">
						email :&nbsp;
					</div>
					<div class="reponse">
						<input type="email" name="email" value=<?php echo $auteur['email'] ?> />
					</div>
					<BR>
					<div class="question">
						sexe :&nbsp;
					</div>
					<div class="reponse">
						<?php
							if ($auteur['sexe']=="M"){
								$masculin="checked";
								$feminin ="";
							}
							else{				
								$masculin="";
								$feminin ="checked";
							}
						?>
						<input type="radio" name="sexe" value="M" <?php echo $masculin ?> > Masculin &nbsp;
						<input type="radio" name="sexe" value="F" <?php echo $feminin ?> > Féminin
					</div>
					<BR><BR><BR>
					<CENTER>
						<input id="choixcv" type ="file" name="cv" accept=".pdf">
						<img id="nouveaucv" src="" alt="Sélectionnez votre C.V." width="50" border="1"><br>
					</CENTER>
					<BR>
					<input type="submit" value="Enregistrer" />&nbsp;
					<input id="hideformulaire" type="reset" value="Abandonner" />&nbsp;
				</form>
			</div>

		</div>
	</div>

	</body>

</html>